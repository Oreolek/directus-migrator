<?php
use voku\db\DB;

require 'vendor/autoload.php';

$client = \Directus\SDK\ClientFactory::create(
    'Pqzh2vnuAFCzP6QWKNeNSthluINBUqEa', [
        'base_url' => 'https://director.oreolek.ru/',
        'version' => 1
    ]
);
$db = DB::getInstance('localhost', 'root', '123456', 'oreolek');

function get_active($draft)
{
    if ($draft == '1')
        return 2;
    return 1;
}

function import_posts($db, $client)
{
    $posts = $db->query("SELECT * FROM posts")->fetchAllArray();
    foreach($posts as $post)
    {
        echo $post['name'].PHP_EOL;
        try {
            $client->createEntry('posts', [
                'title' => $post['name'],
                'active' => get_active($post['is_draft']),
                'published_at' => $post['posted_at'],
                'password' => $post['password'],
                'ID' => $post['id'],
                'content' => $post['content'],
            ]);
        } catch (Exception $e) {
            continue;
        }
    }
}

function import_tags($db, $client)
{
    $result = $db->query("SELECT * FROM tags")->fetchAllArray();
    $tags = [];
    foreach($result as $row)
    {
        $tags[$row['id']] = $row['name'];
    }
    $result = $db->query("SELECT * FROM posts_tags")->fetchAllArray();
    foreach($result as $row)
    {
        $tag = $tags[$row['tag_id']];
        echo $tag.PHP_EOL;
        try {
            $post_tags = $client->getEntry('posts', $row['post_id'])->tags;
            if (!empty($post_tags))
            {
                $post_tags = $post_tags . ", " . $tag;
            } else {
                $post_tags = $tag;
            }
            $client->updateEntry('posts', $row['post_id'], [
                'tags' => $post_tags,
            ]);
        } catch (Exception $e) {
            continue;
        }
    }
}

function import_pages($db, $client)
{
    $pages = $db->query("SELECT * FROM pages")->fetchAllArray();
    foreach($pages as $page)
    {
        echo $page['name'].PHP_EOL;
        try {
            $client->createEntry('pages', [
                'title' => $page['name'],
                'active' => get_active($page['is_draft']),
                'ID' => $page['id'],
                'content' => $page['content'],
            ]);
        } catch (Exception $e) {
            continue;
        }
    }
}

// import_posts($db, $client);
// import_pages($db, $client);
import_tags($db, $client);
